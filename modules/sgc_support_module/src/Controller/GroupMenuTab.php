<?php

namespace Drupal\sgc_support_module\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides group content edit functionality.
 */
class GroupMenuTab extends ControllerBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The current user trying to access the group menu tab.
   *
   * @var \Drupal\sgc_support_module\Controller\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a new GroupMembershipController.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger for logging errors or warnings.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger,
    AccountProxyInterface $account
  ) {
    $this->logger = $logger;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('current_user')
    );
  }

  /**
   * Checks that requested group menu exists and user has access.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The unique profile ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(GroupInterface $group)
  : AccessResultInterface {
    $group_menu = self::loadGroupMenu($group);

    // Allow certain users to bypass this access check.
    if (
      $this->account->id() == 1
      || $this->account->hasPermission('bypass group access')
      || $this->account->hasPermission('administer group content menu types')
    ) {
      return AccessResult::allowed()
        ->addCacheableDependency($group_menu)
        ->addCacheableDependency($group)
        ->cachePerUser();
    }

    return GroupAccessResult::allowedIfHasGroupPermissions(
      $group,
      $this->account,
      [
        'access group content menu overview',
        'manage group_content_menu',
        'manage group_content_menu menu items'
      ],
      'OR')
      ->addCacheableDependency($group_menu)
      ->addCacheableDependency($group)
      ->cachePerUser()
      ;
  }

  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return mixed
   *   A redirect or a blank render array.
   */
  public function editMenuForward(GroupInterface $group) {
    $group_menu = self::loadGroupMenu($group);

    // If the group has a node, redirect them to it's edit form.
    if ($group_menu) {
      $group_menu = $group_menu->getEntity();
      $return_destination = '/group/' . $group->id();
      // @todo: Get return destination to work.
      return $this->redirect(
        'entity.group_content_menu.edit_form',
        [
          'group' => $group->id(),
          'group_content_menu' => $group_menu->id()
        ],
      );
    }
    else {
      $message = $this->t('User attempted to edit group menu for group @GID, but a menu was unable to be located.', [
        '@GID' => $group->id(),
      ]);
      $this->logger->get('sgc_support_module')->error($message);

      return [
        '#markup' => 'There was an issue with loading the menu for this group.',
      ];
    }
  }

  /**
   * Abstract the loading of a group's menu
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   A fully qualified group in which to look for rendered content / homepage.
   *
   * @return \Drupal\group\Entity\GroupContentInterface
   */
  private static function loadGroupMenu(GroupInterface $group) {
    $group_menus = group_content_menu_get_menus_per_group($group);

    // Return the first array if there is one.
    if (array($group_menus) && count($group_menus) === 1) {
      return array_values($group_menus)[0];
    }

    // We either have no menu or multiple. We don't want the tab either way.
    return FALSE;
  }

}
