<?php

namespace Drupal\sgc_support_module\EventSubscriber;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 *
 * We deny access to profiles, if the user has not accepted the terms.
 *
 * See https://www.drupal.org/node/2187643
 *
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Add custom access check to the group content menu route.
    $gcontent_menu_collection_route = $collection->get('entity.group_content_menu.collection');
    $gcontent_menu_collection_route?->setRequirement('_custom_access', 'Drupal\sgc_support_module\EventSubscriber\RouteSubscriber::gContentMenuAccess');

    $routes_to_add_perm_to = [
      'entity.group_content_menu.add_page',
      'entity.group_content_menu.add_form',
      'entity.group_content_menu.add_menu_link',
    ];

    foreach ($routes_to_add_perm_to as $route_to_add_to) {
      $route = $collection->get($route_to_add_to);
      $route?->setRequirement('_group_permission', 'manage group_content_menu');
    }
  }

  /**
   * Method for restricting access to the Group Content Menu overview route.
   */
  public static function gContentMenuAccess(AccountInterface $account) {
    // Restrict group content menu overview to just admins.
    if (!empty(array_intersect($account->getRoles(), ['admin']))) {
      return AccessResult::forbidden('The group content menu overview is restricted to just admins.');
    }

    return AccessResult::neutral();
  }

}
